#ifndef __LIBFACT_OBIWARP_GW_GW_HXX__
#define __LIBFACT_OBIWARP_GW_GW_HXX__

#ifdef _MSC_VER
#ifdef LIBFACT_OBIWARP_GW_GW_EXPORTS
#define LIBFACT_OBIWARP_GW_GW_IMPEXP __declspec(dllexport)
#else
#define LIBFACT_OBIWARP_GW_GW_IMPEXP __declspec(dllimport)
#endif
#else
#define LIBFACT_OBIWARP_GW_GW_IMPEXP
#endif

extern "C" LIBFACT_OBIWARP_GW_GW_IMPEXP int libfact_obiwarp_gw(wchar_t* _pwstFuncName);

CPP_GATEWAY_PROTOTYPE(sci_obiwarp);

#endif /* __LIBFACT_OBIWARP_GW_GW_HXX__ */
