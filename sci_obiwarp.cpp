#include "double.hxx"
#include "string.hxx"
#include "function.hxx"

extern "C"
{
#include "Scierror.h"
#include "sciprint.h"
#include "localization.h"
#include "api_scilab.h"   //JC
}

#ifdef _MSC_VER
__declspec(dllimport)
#endif
extern "C"
{
 int obiwarp(
   char *file1, char *file2,
   char *format,
   char *outfile,
   int images,
   char *timefile,
   char *score,
   int local,
   int nostdnrm,
   float factor_diag, float factor_gap,
   float gap_init, float gap_extend,
   float init_penalty,
   float response,
   char *smat_in, char* smat_out);
}


static const char* fname = "obiwarp";

static char* getStringArg(types::typed_list &in, int iOpt) {
    if (in[iOpt]->isString() == false)
    {
        Scierror(999, _("%s: Wrong type for input argument #%d: string expected.\n"), fname, iOpt+1);
        return nullptr;
    }
    types::String* arg = in[iOpt]->getAs<types::String>();
    return wide_string_to_UTF8(arg->get(0));
}

template<typename T>
static T getDoubleArg(types::typed_list &in, int iOpt) {
    if (in[iOpt]->isDouble() == false)
    {
        Scierror(999, _("%s: Wrong type for input argument #%d: real expected.\n"), fname, iOpt+1);
        return (T) -1;
    }
    types::Double* arg = in[iOpt]->getAs<types::Double>();
    return (T) (arg->get(0));
}

types::Function::ReturnValue sci_obiwarp(types::typed_list &in, int _iRetCount, types::typed_list &out) {
    char *file1 = nullptr;
    char *file2 = nullptr;
    char *format = nullptr;
    char *outfile = nullptr;
    int images = 0;
    char *timefile = nullptr;
    char *score = "cor";
    int local = 0;
    int nostdnrm = 0;
    float factor_diag = 2.0f;
    float factor_gap = 1.0f;
    float gap_init = 0.0f;
    float gap_extend = 0.0f;
    float init_penalty = 0.0f;
    float response = 1.0f;
    char *smat_in = nullptr;
    char* smat_out = nullptr;

    if (in.size() < 2 || in.size() > 15)
    {
        Scierror(999, _("%s: %d to %d input arguments expected.\n"), fname, 2, 15);
        return types::Function::Error;
    }
    if (out.size() > 1)
    {
        Scierror(999, _("%s: %d output argument expected.\n"), fname, 1);
        return types::Function::Error;
    }

    // Mandatory argument 1: first input file name
    if (in[0]->isString() == false)
    {
        Scierror(999, _("%s: Wrong type for input argument #%d: string expected.\n"), fname, 1);
        return types::Function::Error;
    }
    types::String* f1 = in[0]->getAs<types::String>();
    file1 = wide_string_to_UTF8(f1->get(0));
    //sciprint("file1 = %s\n", file1);        // impression console supprimee le 23 aout18 

    // Mandatory argument 2: second input file name
    if (in[1]->isString() == false)
    {
        Scierror(999, _("%s: Wrong type for input argument #%d: string expected.\n"), fname, 2);
        return types::Function::Error;
    }
    types::String* f2 = in[1]->getAs<types::String>();
    file2 = wide_string_to_UTF8(f2->get(0));
    //sciprint("file2 = %s\n", file2);

    // Check input files existence
    FILE *fpt = fopen(file1, "r");
    if (fpt == nullptr) {
        Scierror(999, _("%s: Wrong value for input argument #%d: an existing file expected.\n"), fname, 1);
        return types::Function::Error;
    }
    //fclose(fpt);
    fpt = fopen(file2, "r");
    if (fpt == nullptr) {
        Scierror(999, _("%s: Wrong value for input argument #%d: an existing file expected.\n"), fname, 2);
        return types::Function::Error;
    }
    //fclose(fpt);

    // Optional argument 1: format
    int iOpt = 2;
    if (in.size() > iOpt && !(in[iOpt]->isDouble() == true && in[iOpt]->getAs<types::Double>()->isEmpty())) {
        if ((format = getStringArg(in, iOpt)) == nullptr) {
            return types::Function::Error;
        }
    }
    //sciprint("format = %s\n", format);

    // Optional argument 2: outfile
    iOpt++;
    if (in.size() > iOpt && !(in[iOpt]->isDouble() == true && in[iOpt]->getAs<types::Double>()->isEmpty())) {
        if ((outfile = getStringArg(in, iOpt)) == nullptr) {
            return types::Function::Error;
        }
    }
    //sciprint("outfile = %s\n", outfile);

    // Optional argument 3: images
    iOpt++;
    if (in.size() > iOpt && !(in[iOpt]->isDouble() == true && in[iOpt]->getAs<types::Double>()->isEmpty())) {
        if ((images = getDoubleArg<int>(in, iOpt)) == (int) -1) {
            return types::Function::Error;
        }
    }
    //sciprint("images = %d\n", images);

    // Optional argument 4: timefile
    iOpt++;
    if (in.size() > iOpt && !(in[iOpt]->isDouble() == true && in[iOpt]->getAs<types::Double>()->isEmpty())) {
        if ((timefile = getStringArg(in, iOpt)) == nullptr) {
            return types::Function::Error;
        }
    }
    //sciprint("timefile = %s\n", timefile);

    // Optional argument 5: score
    iOpt++;
    if (in.size() > iOpt && !(in[iOpt]->isDouble() == true && in[iOpt]->getAs<types::Double>()->isEmpty())) {
        if ((score = getStringArg(in, iOpt)) == nullptr) {
            return types::Function::Error;
        }
    }
    //sciprint("score = %s\n", score);

    // Optional argument 6: local
    iOpt++;
    if (in.size() > iOpt && !(in[iOpt]->isDouble() == true && in[iOpt]->getAs<types::Double>()->isEmpty())) {
        if ((local = getDoubleArg<int>(in, iOpt)) == (int) -1) {
            return types::Function::Error;
        }
    }
    //sciprint("local = %d\n", local);

    // Optional argument 7: nostdnrm
    iOpt++;
    if (in.size() > iOpt && !(in[iOpt]->isDouble() == true && in[iOpt]->getAs<types::Double>()->isEmpty())) {
        if ((nostdnrm = getDoubleArg<int>(in, iOpt)) == (int) -1) {
            return types::Function::Error;
        }
    }
    //sciprint("nostdnrm = %d\n", nostdnrm);

    // Optional argument 8: factor
    iOpt++;
    if (in.size() > iOpt && !(in[iOpt]->isDouble() == true && in[iOpt]->getAs<types::Double>()->isEmpty())) {
        if (in[iOpt]->isDouble() == false)
        {
            Scierror(999, _("%s: Wrong type for input argument #%d: real expected.\n"), fname, iOpt+1);
            return types::Function::Error;
        }
        types::Double* factor = in[iOpt]->getAs<types::Double>();
        if (factor->getSize() != 2)
        {
            Scierror(999, _("%s: Wrong size for input argument #%d: a vector of %d elements expected.\n"), fname, iOpt+1, 2);
            return types::Function::Error;
        }
        factor_diag = (float) (factor->get(0));
        factor_gap = (float) (factor->get(1));
    }
    //sciprint("factor_diag = %3.2f\n", factor_diag);
    //sciprint("factor_gap = %3.2f\n", factor_gap);

    // Optional argument 9: gap
    iOpt++;
    if (in.size() > iOpt && !(in[iOpt]->isDouble() == true && in[iOpt]->getAs<types::Double>()->isEmpty())) {
        if (in[iOpt]->isDouble() == false)
        {
            Scierror(999, _("%s: Wrong type for input argument #%d: real expected.\n"), fname, iOpt+1);
            return types::Function::Error;
        }
        types::Double* gap = in[iOpt]->getAs<types::Double>();
        if (gap->getSize() != 2)
        {
            Scierror(999, _("%s: Wrong size for input argument #%d: a vector of %d elements expected.\n"), fname, iOpt+1, 2);
            return types::Function::Error;
        }
        gap_init = (float) (gap->get(0));
        gap_extend = (float) (gap->get(1));
    }
    //sciprint("gap_init = %3.2f\n", gap_init);
    //sciprint("gap_extend = %3.2f\n", gap_extend);

    // Optional argument 10: init_penalty
    iOpt++;
    if (in.size() > iOpt && !(in[iOpt]->isDouble() == true && in[iOpt]->getAs<types::Double>()->isEmpty())) {
        if ((init_penalty = getDoubleArg<float>(in, iOpt)) == (float) -1) {
            return types::Function::Error;
        }
    }
    //sciprint("init_penalty = %3.2f\n", init_penalty);

    // Optional argument 11: response
    iOpt++;
    if (in.size() > iOpt && !(in[iOpt]->isDouble() == true && in[iOpt]->getAs<types::Double>()->isEmpty())) {
        if ((response = getDoubleArg<float>(in, iOpt)) == (float) -1) {
            return types::Function::Error;
        }
    }
    //sciprint("response = %3.2f\n", response);

    // Optional argument 12: smat_in
    iOpt++;
    if (in.size() > iOpt && !(in[iOpt]->isDouble() == true && in[iOpt]->getAs<types::Double>()->isEmpty())) {
        if ((smat_in = getStringArg(in, iOpt)) == nullptr) {
            return types::Function::Error;
        }
    }
    //sciprint("smat_in = %s\n", smat_in);

    // Optional argument 13: smat_out
    iOpt++;
    if (in.size() > iOpt) {
        if ((smat_out = getStringArg(in, iOpt)) == nullptr) {
            return types::Function::Error;
        }
    }
    //sciprint("smat_out = %s\n", smat_out);


    int ret = obiwarp(file1, file2, format, outfile, images, timefile, score, local, nostdnrm,
        factor_diag, factor_gap, gap_init, gap_extend, init_penalty, response, smat_in, smat_out);


    free(file1);
    free(file2);
    free(outfile);
    free(timefile);
    //free(score);
    free(smat_in);
    free(smat_out);

    out.push_back(new types::Double(ret));
    return types::Function::OK;
}
