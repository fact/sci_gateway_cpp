#include "context.hxx"
#include "cpp_gateway_prototype.hxx"
#include "libfact_cpp.hxx"
extern "C"
{
#include "libfact_cpp.h"
}

#define MODULE_NAME L"libfact_cpp"

int libfact_cpp(wchar_t* _pwstFuncName)
{
    if(wcscmp(_pwstFuncName, L"obiwarp_lmata") == 0){ symbol::Context::getInstance()->addFunction(types::Function::createFunction(L"obiwarp_lmata", &sci_obiwarp, MODULE_NAME)); }

    return 1;
}
